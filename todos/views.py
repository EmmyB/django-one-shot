from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from django.contrib.auth.decorators import login_required
from todos.forms import CreateForm

# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todo_lists/list.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list": list,
    }
    return render(request, "todo_lists/detail.html", context)


# @login_required
def todo_list_create(request):
    if request.method=="POST":
        form=CreateForm(request.POST)
        if form.is_valid():
            form.save()
            # list.author=request.user
            list=form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form=CreateForm()
    context={
        "form": form
    }
    return render(request, "todo_lists/create.html", context)


def todo_list_update(request, id):
    list=get_object_or_404(TodoList, id=id)
    if request.method=="POST":
        form=CreateForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form=CreateForm()
    context={
        "form": form
    }
    return render(request, "todo_lists/edit.html", context)



def todo_list_delete(request, id):
    list=TodoList.objects.get(id=id)
    if request.method=="POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todo_lists/delete.html")